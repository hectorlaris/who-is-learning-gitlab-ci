# Who is learning GitLab CI?

## Getting started

- Watch the [GitLab CI course on freecodecamp](https://youtu.be/PGyhBwLyK2U)
- Check the [course notes](https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci/-/blob/main/docs/course-notes.md)
